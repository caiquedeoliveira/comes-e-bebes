<?php

    add_action('wp_enqueue_scripts', 'myStyles');
    add_action('wp_enqueue_scripts', 'myScripts');

    function myStyles(){
        wp_enqueue_style( 'style.css', get_template_directory_uri().'/style.css');
    }

    function myScripts(){
        wp_enqueue_script('showsidebar', get_template_directory_uri() . '/scripts/sidebar.js');
    }

    function mytheme_add_woocommerce_support() {
        add_theme_support( 'woocommerce' );
    }
    add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );
    add_filter( 'woocommerce_get_related_product_tag_terms', function( $term_ids, $product_id ){
        return array();
    }, 10, 2 );


    add_action( 'init', 'bc_remove_wc_breadcrumbs' );
    function bc_remove_wc_breadcrumbs() {
        remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
    }

?>

<?php 


    remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash');

    remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
    function action_woocommerce_template_loop_price() {
        global $product;
        echo "<p>R$ ";
        if ($product->is_type( 'variable' )) {

            $product_variations = $product->get_available_variations();
            

            $variation_product_id_max = end($product_variations)['variation_id'];
            $variation_product_id_min = $product_variations [0]['variation_id'];


            $variation_product_max = new WC_Product_Variation( $variation_product_id_max );
            $variation_product_min = new WC_Product_Variation( $variation_product_id_min );


            echo $variation_product_max ->regular_price;
            echo $variation_product_max ->sale_price;
            echo " - ";
            echo $variation_product_min ->regular_price;
            echo $variation_product_min ->sale_price;
        }

        else
            {if( $product->is_on_sale() ) {
                echo "<strike>" . $product->get_regular_price() . "</strike> &rarr; ";
                echo $product->get_sale_price();
            }
            else {echo $product->get_regular_price();}}

        echo "  </p><button onclick='window.location = `?add-to-cart={$product->id}`'><img src='" . get_stylesheet_directory_uri() . "/img/icone-buy.png'></button>";
    }
    add_action('woocommerce_after_shop_loop_item_title', 'action_woocommerce_template_loop_price', 10 );


    remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
    function action_woocommerce_shop_loop_item_title() {
        $output =   "   <div class='categoria-label'>
                            <p>" . get_the_title() . "</p>
                            <div class='container-preco-icon'>";

        echo $output; 
    }
    add_action( 'woocommerce_shop_loop_item_title', 'action_woocommerce_shop_loop_item_title', 10 );


    remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);

    add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);

    if (!function_exists('woocommerce_template_loop_product_thumbnail')) {

        function woocommerce_template_loop_product_thumbnail()
        {
            echo woocommerce_get_product_thumbnail();
        }
    }


    if (!function_exists('woocommerce_get_product_thumbnail')) {

        /**
         * @param string 
         * @param int 
         * @param int 
         * @return string
         */
        function woocommerce_get_product_thumbnail($size = 'shop_catalog', $placeholder_width = 0, $placeholder_height = 0)
        {
            global $post, $woocommerce;
            

            $placeholder_width = !$placeholder_width ?
                wc_get_image_size('shop_catalog_image_width')[ 'width' ] :
                $placeholder_width;

            $placeholder_height = !$placeholder_height ?
                wc_get_image_size('shop_catalog_image_height')[ 'height' ] :
                $placeholder_height;

        
            $output .= "<div class='container-secao-2-item-template produto' style='background-image: url(" . get_the_post_thumbnail_url() . ");'>";

            return $output;
        }}
?>

<?php 


    
    add_action( 'woocommerce_after_quantity_input_field', 'silva_display_quantity_plus' );
    
    function silva_display_quantity_plus() {
    echo '<button type="button" class="plus" >+</button>';
    }
    
    add_action( 'woocommerce_before_quantity_input_field', 'silva_display_quantity_minus' );
    
    function silva_display_quantity_minus() {
    echo '<button type="button" class="minus" >-</button>';
    }
    
    
    add_action( 'wp_footer', 'silva_add_cart_quantity_plus_minus' );
    
    function silva_add_cart_quantity_plus_minus() {
    
    if ( ! is_product() && ! is_cart() ) return;
        
    wc_enqueue_js( "   
            
        $('form.cart,form.woocommerce-cart-form').on( 'click', 'button.plus, button.minus', function() {
    
            var qty = $( this ).parent( '.quantity' ).find( '.qty' );
            var val = parseFloat(qty.val());
            var max = parseFloat(qty.attr( 'max' ));
            var min = parseFloat(qty.attr( 'min' ));
            var step = parseFloat(qty.attr( 'step' ));
    
            if ( $( this ).is( '.plus' ) ) {
                if ( max && ( max <= val ) ) {
                qty.val( max );
                } else {
                qty.val( val + step );
                }
            } else {
                if ( min && ( min >= val ) ) {
                qty.val( min );
                } else if ( val > 1 ) {
                qty.val( val - step );
                }
            }
    
        });
            
    " );
    }

?>

<?php

    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
    remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash');
    remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs');
    
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price');
    

    function woocommerce_template_single_price() {
        global $product;
        if (!$product->is_type( 'variable' )){
            wc_get_template( 'single-product/price.php' );
        }
    }
    add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 100);


    add_action('woocommerce_single_product_summary', 'mostrarDesc', 6);
    function mostrarDesc() {
        echo "<div class='container-descricao-single-product'><p>" . get_the_excerpt() . "</p></div>";
    }


    add_filter('woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_add_to_cart_text');
 
    function woocommerce_custom_add_to_cart_text() {
    return __('Adicionar', 'woocommerce');
    }


    add_action('wp_footer', 'update_variable_product_price', 999, 0);
    function update_variable_product_price() {
        if( ! is_product() ) return;
        ?>
        <script>
            jQuery(function ($) {
                $(document).ready(function() {
                    var original_price_html = $('.wrap-top-price').html();
                    $('.entry-summary').on('click', '.reset_variations', function() {
                        $('.wrap-top-price').html(original_price_html);
                    });
                    $('.variations_form').on('woocommerce_variation_has_changed', function () {
                        if($('.woocommerce-variation-price .woocommerce-Price-amount').html().length) {
                            $('.wrap-top-price').empty();
                            $('.wrap-top-price').html($('.woocommerce-variation-price .woocommerce-Price-amount').html());
                        }
                    });
                })
            });
        </script>
        <?php
    }
    
?>
