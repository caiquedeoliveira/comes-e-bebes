<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'yGVOBBYnlU230KopvLwvFPhtraATL6NVmBaD2dWHZ2noWnrDE3sId1BnLYYo3hdOB6wN+tS2I2QR0DHQJ3eIHA==');
define('SECURE_AUTH_KEY',  '5wWELR7oCHaYBg2pSjJ81/tjPQ/P55h772u+J1+V3oYdpIyS0alhJsQlSsawMWylf8jxm54GcyjMyxKwK3QtxQ==');
define('LOGGED_IN_KEY',    'ut+JMIa2lc4aa1hH/80LduffLwUz1ZCuXsTB9GccbXKT93BFrfabFcd5Sg2YsCvVj2B+1LzOFgkobZlDpsodTQ==');
define('NONCE_KEY',        'ELBcAT2Sndk7sOrunryq1aSmEH0X0aYFQzQHKVuNI08zBnYLtScqaeWyZVEWwePA63d8pgiHJeuLqj2RDXZUKA==');
define('AUTH_SALT',        '/k6itZ6s7AZxik7LfxaVw477Z4EH5oiVy5lEawjCZ7omo4lquL0mqIyTBU0xzAAMz9GayZ/21bHyFK8Txqq1EA==');
define('SECURE_AUTH_SALT', 'i2BIwpLcbPQxCBVZ2U6VU2jdIK75Nr0l98NGXf408ibt6qRWIHIKELR9yTZK0R0raCkTxoWFXphRn06Rqy7Krg==');
define('LOGGED_IN_SALT',   'oB0ndqflu0iFZoBiSvX26Nb/AAb4DVkCgZVBrmjT0JxE9mLMY6w+J7ckfIaeCXaxN0o0Fe4VFdQGCnx9lvqgcQ==');
define('NONCE_SALT',       '8MW3E9IpwdvC8V7ZN5HE9/S0NkjXOIOcDmw5mJcWbSB2CBdITwNotOkuRi+1WPwjCadEv6VAKWmNe+ALL3FYjw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
