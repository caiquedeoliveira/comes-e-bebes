window.addEventListener('DOMContentLoaded', (event) => {
    
    // Listener do botão do carrinho para mostrar sidebar
    const botaoCarrinho = document.querySelector("#botao-carrinho")
    botaoCarrinho.addEventListener("click", (event) => {
        const sidebar = document.querySelector("#sidebar-container")
        const sidebarSection = document.querySelector("#sidebar")
        
        sidebar.classList.toggle("active")
        sidebarSection.classList.toggle("active")
        sidebarOutside.classList.toggle("active")
    })

    // Listener pra sair da sidebar quando clicar fora
    const sidebarOutside = document.querySelector("#sidebar-outside")
    sidebarOutside.addEventListener("click", (event) => {
        if (event.target != document.querySelector("#sidebar")) {
            const sidebar = document.querySelector("#sidebar-container")
            const sidebarSection = document.querySelector("#sidebar")

            sidebar.classList.toggle("active")
            sidebarSection.classList.toggle("active")
            sidebarOutside.classList.toggle("active")
        }
    })

});
